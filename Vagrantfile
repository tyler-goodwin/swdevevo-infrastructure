# -*- mode: ruby -*-
# vi: set ft=ruby :
require('./config')
require('./provision_docker')
require('./provision_jenkins')
require('./setup_virtualbox')

def provision_hosts(config)
  Config::HOSTS.each do | host, ip_addr |
    config.vm.provision 'shell', inline: "grep -q #{host} /etc/hosts || echo #{ip_addr}\t#{host} >> /etc/hosts"
  end
end

Vagrant.configure("2") do |config|
  # CI Server
  config.vm.define "jenkins" do |jenkins|
    jenkins.vm.hostname = "jenkins"
    setup_virtualbox(jenkins, 6096, 4)
    provision_with_docker(jenkins)
    provision_jenkins(jenkins)
    provision_hosts(jenkins)
    jenkins.vm.provision 'shell', inline: 'sudo usermod -aG docker jenkins'
    jenkins.vm.network "forwarded_port", guest: 8080, host: 8080
    jenkins.vm.network "private_network", ip: Config::HOSTS[:jenkins]
  end

  # Staging Server
  config.vm.define "staging" do |staging|
    staging.vm.hostname = "staging"
    setup_virtualbox(staging)
    provision_with_docker(staging)
    provision_hosts(staging)
    staging.vm.network "private_network", ip: Config::HOSTS[:staging]
  end

  # Production Server
  config.vm.define "production" do |prod|
    prod.vm.hostname = "production"
    setup_virtualbox(prod)
    provision_with_docker(prod)
    provision_hosts(prod)
    prod.vm.network "private_network", ip: Config::HOSTS[:production]
  end

  # Production Monitoring/Instrumentation
  config.vm.define "grafana" do |grafana|
    grafana.vm.hostname = "grafana"
    grafana.vm.synced_folder "./grafana", "/grafana",
    setup_virtualbox(grafana)
    provision_with_docker(grafana)
    provision_hosts(grafana)
    grafana.vm.network "private_network", ip: Config::HOSTS[:grafana]
  end
end
