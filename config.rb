class Config
  HOSTS = { 
    jenkins:    "192.168.10.1",
    staging:    "192.168.10.2",
    production: "192.168.10.3", 
    grafana:    "192.168.10.4"
  }
end